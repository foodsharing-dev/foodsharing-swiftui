//
//  API.swift
//  foodsharing-swiftui
//
//  Created by Jannes Münse on 01.12.22.
//

import Foundation
import Models

import SwiftUI
import os.log

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}

@MainActor
public class API: ObservableObject {
    // MARK: Settings
    @AppStorage(Defaults.baseURL) private(set) var baseURL = URL(string: "https://beta.foodsharing.de/")!
    
    // MARK: Variables
    @Published var csrfToken: HTTPCookie?
    @Published var phpSessId: HTTPCookie?
    @Published public var user: LoginResponse?
    
    
    public var isLoggedIn: Bool { csrfToken != nil && user != nil }
    let urlSession: URLSession
    let logger = Logger(subsystem: "de.foodsharing.Foodsharing", category: "API")
    
    public let decoder: JSONDecoder
    public let encoder: JSONEncoder

    
    // MARK: - init
    
    public init() {
        urlSession = URLSession.shared
        
        decoder = JSONDecoder()
        encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
    }
    
    // MARK: - Networking
    
     func request(path: String, httpMethod: HTTPMethod, body: Data?) async throws -> Data  {
        let url = baseURL.appending(path: path)
        var request = URLRequest(url: url)
        
        if let body {
            request.httpBody = body
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }

        
        request.httpMethod = httpMethod.rawValue
        let (data, response) = try await urlSession.data(for: request)
        guard let urlResponse = response as? HTTPURLResponse else {
            throw APIError.unknown
        }
        let statusCode = urlResponse.statusCode
        logger.debug("status code: \(statusCode)")
        switch statusCode {
        case 200...202:
            break
        case 401...403:
            throw APIError.notAuthorized
        default:
            let errorResponse = try decoder.decode(Response.self, from: data)
            throw APIError.status(code: statusCode, reason: errorResponse.reason)
        }
        return data
    }
}

    //MARK: - extentions
enum APIError: LocalizedError {
    case invalidBaseURL
    case invalidCredentials
    case notAuthorized
    case status(code: Int, reason: String)
    case unknown
    
    var errorDescription: String? {
        switch self {
        case .invalidBaseURL:
            return "Invalid base URL"
        case .invalidCredentials:
            return "Invalid credentials"
        case .notAuthorized:
            return "Not authorized."
        case .status(let code, let reason):
            return "Error \(code). Reason: \(reason)"
        case .unknown:
            return "An unknown error occured."
        }
    }
}
extension APIError: Equatable {}

 struct Response: Decodable {
    let error: Bool
    let reason: String
}

enum Defaults {
    static let baseURL = "baseURL"
    static let showWeekend = "showWeekend"
    static let dayPreviewUserCount = "dayPreviewUserCount"
    static let showReservationButton = "showReservationButton"
    static let csrfToken = "CSRF_TOKEN"
    static let user = "user"
    static let pspSessId = "PHPSESSID"
}
