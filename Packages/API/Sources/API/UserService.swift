//
//  UserService.swift
//  foodsharing-swiftui
//
//  Created by Jannes Münse on 13.12.22.
//

import Foundation
import Models

extension API {
    
// MARK: - user
public func login(loginData: LoginData) async {
    do {
        let decodedUser = try encoder.encode(loginData)
        if let data = try? await request(path: "api/user/login", httpMethod: .post, body: decodedUser)
        {
            try parseUserAndToken(data: data)
        }
    } catch {print(error)}
}

public func logout() async {
    do {
        let data = try await request(path: "api/user/logout",httpMethod: .post, body: nil)
        logger.debug("data is: \(data)")
    }catch{print(error)}
    let foodsharingCookies = urlSession.configuration.httpCookieStorage?.cookies(for: baseURL)
    foodsharingCookies?.forEach({cookie in
        urlSession.configuration.httpCookieStorage?.deleteCookie(cookie)
    })
    csrfToken = nil;
    user = nil;
}

private func parseUserAndToken(data: Data) throws {
    let cookies = urlSession.configuration.httpCookieStorage?.cookies(for: baseURL)
    let crsftcookie = cookies?.first(where: {$0.name == Defaults.csrfToken})
    let phpSessIdCookie = cookies?.first(where: {$0.name == Defaults.pspSessId})
    csrfToken = crsftcookie
    phpSessId = phpSessIdCookie
    let decodedUser = try decoder.decode(LoginResponse.self, from: data)
    user = decodedUser
}

public func getCurrentUserDetails() async -> UserDetailsResponse? {
    do {
        if let data = try? await request(path: "api/user/current/details",httpMethod: .get, body: nil)
        {
            let decodedResponse = try decoder.decode(UserDetailsResponse.self, from: data)
            logger.debug("decoded user is: \(data)")
            return decodedResponse
        }
    } catch{print(error)}
    return nil
}
}
