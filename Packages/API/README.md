# API

The API Package contains the API implementation that is used in the Foodsharing app
This allows the API to be built and tested individually.

As the rest api documentation is dividing the calls by content, in this package each context (user, basket, ...) should be an individual file to improve developing and the overview.
