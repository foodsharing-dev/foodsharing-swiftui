//
//  LoginData.swift
//  foodsharing-swiftui
//
//  Created by Jannes Münse on 01.12.22.
//

import Foundation

public class LoginData: Codable {
    public var email: String
    public var password: String
    public var rememberMe: Bool
    
    public init(email: String, password: String, rememberMe: Bool)  {
        self.email = email
        self.password = password
        self.rememberMe = rememberMe
    }
}
