//
//  User.swift
//  foodsharing-swiftui
//
//  Created by Jannes Münse on 01.12.22.
//

import Foundation


public class LoginResponse: Codable, Identifiable {
    public var id: Int
    public var name: String
    public var avatar: String?
    public var sleepStatus: Int

    public init(id: Int, name: String, avatar: String, sleepStatus: Int) {
        self.id = id
        self.name = name
        self.avatar = avatar
        self.sleepStatus = sleepStatus
    }
    
}
