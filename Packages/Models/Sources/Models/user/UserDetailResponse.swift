//
//  UserDetailResponse.swift
//  foodsharing-swiftui
//
//  Created by Jannes Münse on 01.12.22.
//

import Foundation

// MARK: - UserDetailsResponse
public struct UserDetailsResponse: Codable {
    public let id: Int
    public let foodsaver, isVerified: Bool
    public let regionId: Int
    public let regionName, aboutMePublic: String
    public let mailboxId, hasCalendarToken: String?
    public let firstname, lastname: String
    public let gender: Int
    public let photo: String?
    public let sleeping: Bool
    public let homepage: String
    public let stats: Stats
    public let permissions: Permissions
    public let coordinates: Coordinates
    public let address, city, postcode, email: String
    public let landline, mobile, birthday, aboutMeIntern: String

    enum CodingKeys: String, CodingKey {
        case id, foodsaver, isVerified
        case regionId
        case regionName, aboutMePublic
        case mailboxId
        case hasCalendarToken, firstname, lastname, gender, photo, sleeping, homepage, stats, permissions, coordinates, address, city, postcode, email, landline, mobile, birthday, aboutMeIntern
    }
}

// MARK: - Coordinates
public struct Coordinates: Codable {
    let lat, lon: Double
}

// MARK: - Permissions
public struct Permissions: Codable {
    let mayEditUserProfile, mayAdministrateUserProfile, administrateBlog, editQuiz: Bool
    let handleReports, addStore, manageMailboxes, editContent: Bool
    let administrateNewsletterEmail, administrateRegions: Bool
}

// MARK: - Stats
public struct Stats: Codable {
    let weight, count: Int
}
