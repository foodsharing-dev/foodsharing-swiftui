### Build instruction ###

1. Navigate to the Project settings -> Signing & capabilities
2. On 'Team' select your personal developer account
3. Selct a device as build target (e.g. the Simulator)
4. Build the Project

### App usage ###

In the API class you can find the base URL that will be used for all calls to the backend.
By default this URL is 'beta.foodsharing.de'. If you want to run the App against you local backend you need to change it.

To login use your user credentials.
On successfull login you should be redirected to the Dashboard.
You can verify the login was successful if your name appears in the greeting message on the dashboard, since this information is loaded from the login respose.
If you tap the user icon at the top right and then on the 'viewProfile' button you are redirected to the profile view.
You can use the button present on this page to get the user details of the current user.


### Cookies ###
 
On http calls SwiftUI automatically stores cookies for a specific origin and uses them on all calls to the same origin, unless told otherwise.
