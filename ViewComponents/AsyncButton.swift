//
//  AsyncButton.swift
//  foodsharing-swiftui
//
//  Code by Melvin Gundlach
//  Created by Jannes Münse on 01.12.22.
//

import SwiftUI

struct AsyncButton<Label: View>: View {
    private let role: ButtonRole?
    @Binding private var error: Error?
    private let action: Action
    private let label: () -> Label
    
    @State private var isRunning = false
    
    init(role: ButtonRole? = nil, error: Binding<Error?>, action: @escaping Action, label: @escaping () -> Label) {
        self.role = role
        self._error = error
        self.action = action
        self.label = label
    }
    
    var body: some View {
        Button(role: role) {
            Task {
                isRunning = true
                do {
                    try await action()
                } catch {
                    self.error = error
                }
                isRunning = false
            }
        } label: {
            label()
        }
        .overlay(progressView)
        .disabled(isRunning)
    }
    
    @ViewBuilder
    private var progressView: some View {
        if isRunning {
            ProgressView()
                .controlSize(controlSize)
        }
    }
    
    private var controlSize: ControlSize {
        #if os(iOS)
        return .regular
        #else
        return .small
        #endif
    }
    
    typealias Action = @MainActor @Sendable () async throws -> Void
}

extension AsyncButton where Label == Text {
    init(_ titleKey: LocalizedStringKey, role: ButtonRole? = nil, error: Binding<Error?>, action: @escaping Action) {
        self.role = role
        self._error = error
        self.action = action
        self.label = { Text(titleKey) }
    }
    
    init<S: StringProtocol>(_ title: S, role: ButtonRole? = nil, error: Binding<Error?>, action: @escaping Action) {
        self.role = role
        self._error = error
        self.action = action
        self.label = { Text(title) }
    }
}

private struct Demo: View {
    @State private var error: Error?
    
    var body: some View {
        VStack {
            AsyncButton("Normal", error: $error) {
                try await Task.sleep(nanoseconds: 2_000_000_000)
            }
        }
        .buttonStyle(.borderedProminent)
    }
}

struct AsyncButton_Previews: PreviewProvider {
    @State private static var error: Error?
    
    static var previews: some View {
        Demo()
    }
}
