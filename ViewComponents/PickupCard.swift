//
//  SavingCard.swift
//  foodsharing-swiftui
//
//  Created by Jannes Münse on 05.12.22.
//

import SwiftUI

struct PickupCard: View {
    var dateTime: String
    var locationName: String
    var backgroundColor: Color? = nil
    
    @Environment(\.colorScheme) private var colorScheme

    var body: some View {
        ZStack{
            roundedBackground
            VStack {
                HStack {
                    Label("", systemImage: "person.circle")
                    Label("", systemImage: "person.circle")
                    Label("", systemImage: "person.circle")
                    Label("", systemImage: "person.circle")
                    Spacer()
                    Label("", systemImage: "checkmark.circle")
                }
                Text(dateTime)
                Text(locationName)
            }
        }
    }
    
    private var roundedBackground: some View {
        RoundedRectangle(cornerRadius: 12, style: .continuous)
            .foregroundColor(actualBackgroundColor)
            .shadow(radius: 4)
    }
    
    private var actualBackgroundColor: Color {
        if let backgroundColor {
            return backgroundColor
        }
        switch colorScheme {
        case .light: return .white
        case .dark: return .white.opacity(0.1)
        @unknown default: return .white.opacity(0.1)
        }
    }
}

struct SavingCard_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            PickupCard(dateTime: "Dienstag 16:30 Uhr", locationName: "Supermarkt Edeka")
            PickupCard(dateTime: "Mittwoch 14:30 Uhr", locationName: "Marktplatz Wandsbek")
        }
        .frame(width: 300, height: 200)
    }
}
