//
//  foodsharing_App.swift
//  foodsharing-swiftui
//
//  Created by Jannes Münse on 01.12.22.
//

import SwiftUI
import API

@main
struct foodsharing_iosApp: App {
    @StateObject private var api = API()

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(api)
        }
    }
}
