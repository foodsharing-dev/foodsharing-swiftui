//
//  ContentView.swift
//  foodsharing-swiftui
//
//  Created by Jannes Münse on 01.12.22.
//

import SwiftUI
import API



struct ContentView: View {
    @EnvironmentObject private var api: API

    var body: some View {
        if api.isLoggedIn {
            AppTabView()
        } else {
            LoginView()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(API())
    }
}
