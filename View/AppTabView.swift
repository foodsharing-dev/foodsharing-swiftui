//
//  AppTabView.swift
//  foodsharing-swiftui
//
//  Created by Jannes Münse on 01.12.22.
//

import SwiftUI
import API

struct AppTabView: View {
    @EnvironmentObject private var api: API

        
    var body: some View {
        TabView {
            HomeView()
            .tabItem{
                Label("Home", systemImage: "house")
            }
            VStack {
                
            }
            .tabItem{
                Label("Karten", systemImage: "map")
            }
            VStack {
            }
            .tabItem{
                Label("Austausch", systemImage: "square.stack.3d.up.fill")
            }
            VStack {
                
            }
            .badge(1)
            .tabItem{
                Label("Chat", systemImage: "bubble.left.and.bubble.right.fill")
            }
        }
    }
}

struct HomeScreenView_Previews: PreviewProvider {
    static var previews: some View {
        AppTabView()
            .environmentObject(API())
    }
}
