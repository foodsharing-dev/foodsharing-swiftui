//
//  ProfileView.swift
//  foodsharing-swiftui
//
//  Created by Jannes Münse on 01.12.22.
//

import SwiftUI
import API
import Models

struct ProfileView: View {
    @EnvironmentObject private var api: API
    @State private var error: Error?
    @State private var userDetailResponse: UserDetailsResponse?

    var body: some View {
        NavigationStack{
            AsyncButton("getDetails", error: $error){
                await getDetails()
            }
            .buttonStyle(.bordered)
            if (userDetailResponse != nil) {
                Text("firstname = \(userDetailResponse!.firstname)")
                Text("lastname = \(userDetailResponse!.lastname)")
                Text("address = \(userDetailResponse!.address)")
                Text("city = \(userDetailResponse!.city)")
                Text("birthday = \(userDetailResponse!.birthday)")
            }
        }
        .navigationTitle("Profil")
    }
    
}

extension ProfileView {
    func getDetails() async {
        userDetailResponse = await api.getCurrentUserDetails()
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView()
            .environmentObject(API())
    }
}
