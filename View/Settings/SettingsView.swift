//
//  SettingsView.swift
//  foodsharing-swiftui
//
//  Created by Jannes Münse on 01.12.22.
//

import SwiftUI
import API

struct SettingsView: View {
    @EnvironmentObject private var api: API
    
    @State private var isRunning = false
    @State private var error: Error?

    var body: some View {
        NavigationStack {
            VStack {
                Spacer()
                NavigationLink(destination: ProfileView()){
                    Text("view Profile")
                }
                Spacer()
                AsyncButton("Logout",role: .destructive, error: $error) {
                    isRunning = true // isRunning still required to disable onSubmit action
                    defer { Task { @MainActor in isRunning = false }}
                    await api.logout()
                }
                .buttonStyle(.bordered)
                Spacer()
            }
            .navigationTitle("Einstellungen")
        }
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
            .environmentObject(API())
    }
}
