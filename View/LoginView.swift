//
//  LoginView.swift
//  foodsharing-swiftui
//
//  Created by Jannes Münse on 01.12.22.
//

import SwiftUI
import API
import Models

struct LoginView: View {
    @EnvironmentObject private var api: API
    
    @State private var isRunning = false
    @State private var error: Error?
    @State private var username: String = ""
    @State private var password: String = ""

    var body: some View {
            VStack(spacing: 20){
                Spacer()
                Image("foodsharing")
                    .resizable()
                    .scaledToFit()
                Spacer()
                TextField("username", text: $username)
                    .textInputAutocapitalization(.never)
                SecureField("password", text: $password)
                AsyncButton("Login" ,error: $error .self) {
                    await login()
                }
                .buttonStyle(.borderedProminent)
                Spacer()
                
            }
            .textFieldStyle(.roundedBorder)
            .padding()
            .frame(maxWidth: 400)
    }
}

extension LoginView {
    private func login() async {
        isRunning = true // isRunning still required to disable onSubmit action
        defer { Task { @MainActor in isRunning = false }}
        let loginData = LoginData(email: username, password: password, rememberMe: true)
        await api.login(loginData: loginData)
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
            .environmentObject(API())
    }
}

