//
//  Homeview.swift
//  foodsharing-swiftui
//
//  Created by Jannes Münse on 01.12.22.
//

import SwiftUI
import API

struct HomeView: View {
    
    @EnvironmentObject private var api: API
    
    @State private var isRunning = false
    @State private var error: Error?
    @State private var showSettings = false

    var body: some View {
        NavigationStack{
            VStack {
                
            }
            .navigationTitle("Hallo \(api.user?.name ?? "undefined user")")
            .toolbar{
                ToolbarItem(placement: .automatic){
                    Button{
                        showSettings = true
                    } label: {
                        Image(systemName: "person.circle")
                    }
                }
            }
        }
        .sheet(isPresented: $showSettings){
            SettingsView()
        }
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
            .environmentObject(API())
    }
}
